let express = require('express')
var cors = require('cors')

let app = express()
let daysRoute = require('./routes/day')
let usersRoute = require('./routes/users')
let codesRoute = require('./routes/codes')
let path = require('path')
let bodyParser = require('body-parser')
let mongoose = require('mongoose')

const server = 'localhost'
const database = 'appoint'
const user = 'devappoint'
const password = 'devappoint15'

app.use(cors());

const API_PREFIX = '/api/'

app.use(bodyParser.json())
app.use(API_PREFIX, daysRoute)
app.use(API_PREFIX, usersRoute)
app.use(API_PREFIX, codesRoute)

mongoose.set('useFindAndModify', false);

mongoose.connect(`mongodb://${user}:${password}@${server}/${database}`, {useUnifiedTopology: true, useNewUrlParser: true})


// Error handlers
app.use((req, res, next) =>{
    res.status(404).send("End point not found!")
})
app.use((err, req, res, next) =>{
    
    console.error(err.stack)
    res.sendFile(path.join(__dirname, 'public/500.html'))
})


const PORT = process.env.PORT || 3000
app.listen(PORT, () => console.info(`Server has started on ${PORT}`))