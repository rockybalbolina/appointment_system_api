let mongoose = require('mongoose')

let daysSchema =  mongoose.Schema({
    date: Number,
    appointments: []
})

module.exports =  mongoose.model('days', daysSchema)