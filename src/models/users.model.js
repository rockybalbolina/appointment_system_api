let mongoose = require('mongoose')

let userSchema =  mongoose.Schema({
    email: String,
    hash: {type: String, bcrypt: true },
    name: String,
    admin: Boolean
})
userSchema.plugin(require('mongoose-bcrypt'));
module.exports =  mongoose.model('user', userSchema)