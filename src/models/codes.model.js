let mongoose = require('mongoose')

let codesSchema =  mongoose.Schema({
    value: String
})
module.exports =  mongoose.model('codes', codesSchema)