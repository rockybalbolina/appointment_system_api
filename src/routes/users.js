let usersModel = require('../models/users.model');
let express = require('express')
const bcrypt = require('bcrypt');
let router = express.Router()

router.post('/login', (req, res) => {
    if (!req.body) {
        return res.status(400).statusMessage('Request body is missing')
    }

    // bcrypt.hash('usuario', 10, function (err, hash) {
    //     if (err) { throw (err); }

    //     bcrypt.compare('mypassword', hash, function (err, result) {
    //         console.log(hash)
    //         if (err) { throw (err); }
    //         //console.log(result);
    //     });
    // });
    usersModel.find({ email: req.body.email }, (err, user) => {
        if (user === null) {
            return res.status(404).send('User not found')
        }

        bcrypt.compare(req.body.password, user[0].hash).then((same) => {
            if (same) {
                return res.status(200).send({ cod: true, user: refactorUser(user[0]) });
            } else {
                // 4 = Incorrect password 
                return res.status(404).send('ERROR_LOGIN_INCORRECT_PASSWORD');
            }
        }).catch((err) => {
            console.log(err)
        });
    })
})

router.post('/dochangename', (req, res) => {
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    usersModel.findOneAndUpdate({_id : req.body.id}, {$set:{name: req.body.newName}}, {new: true}, (err, doc) => {
        if (err) {
            console.log("Something wrong when updating data!");
        }
        return res.status(200).send();
    });


});

router.post('/dochangeemail', (req, res) => {
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    usersModel.findOneAndUpdate({_id : req.body.id}, {$set:{email: req.body.newEmail}}, {new: true}, (err, doc) => {
        if (err) {
            console.log("Something wrong when updating data!");
        }
        return res.status(200).send();
    });


});

router.post('/register', (req, res) => {
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    usersModel.find({ email: req.body.email }, (err, user) => {

        //if user dont exit, hash the password and save it
        if (user.length < 1) {

            bcrypt.hash(req.body.password, 10, function (err, hash) {

                req.body.password = hash;

                if (err) { throw (err); }

            });

            let model = new usersModel(req.body)
            model.save()
                .then(doc => {
                    if (!doc || doc.length === 0) {
                        return res.status(500).send(doc)
                    }
                    res.status(200).send(doc)
                })
                .catch(err => {
                    res.status(500).json(err)
                })
        } else {
            // 5 = User already exist
            return res.status(404).send('5')
        }

    });


})

router.post('/user', (req, res) => {
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    usersModel.find({ _id: req.body.id }).then(user => {

        res.status(200).send(refactorUser(user[0]))

    })
})

// router.get('/users/:id', (req, res) => {
//     res.send(`You have resquested appoint ${req.params.id}`)
// })

router.get('/error', (req, res) => {
    throw new Error('forced error')
})


function refactorUser(userRaw){

    return refacUser = {id: userRaw._id, email: userRaw.email, name: userRaw.name, admin: userRaw.admin };
}

module.exports = router