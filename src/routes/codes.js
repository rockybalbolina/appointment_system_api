let codesModel = require('../models/codes.model');
let express = require('express')
let router = express.Router()

router.post('/codes', (req, res) =>{
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    codesModel.find({value: req.body.code},(err, code) =>{
        if (code.length < 1) {
            // 7 = Code does not exist
            return res.status(404).send('7')
        }else{
            codesModel.deleteOne({value: req.body.code}, (err, code) =>{
                res.status(200).send(code)
            })
        }
    })

})


module.exports = router