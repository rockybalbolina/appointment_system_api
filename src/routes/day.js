let daysModel = require('../models/day.model');
let express = require('express')
let router = express.Router()

router.post('/days', (req, res) =>{
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    let model = new daysModel(req.body)
    model.save()
    .then(doc =>{
        if (!doc || doc.length === 0) {
            return res.status(500).send(doc)          
        }
        res.status(201).send(doc)
    })
    .catch(err =>{
        res.status(500).json(err)
    })
})

router.get('/days', (req, res) => {
    if (!req.body) {
        return res.status(400).send('Request body is missing')
    }
    const today = new Date().getTime();
    const next30 = new Date();
    next30.setDate(next30.getDate() + 30);

    let model = new daysModel()
    
    daysModel.find({ date: { $lte: next30.getTime(), $gte: today}}).then(days => {
        res.status(200).send(days)
    })
})

router.get('/appoint/:id', (req, res) => {

    res.send(`You have resquested appoint ${req.params.id}`)
})

router.get('/error',(req, res) =>{
    throw new Error('forced error')
})

module.exports = router